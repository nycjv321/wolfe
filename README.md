# wolfe v.0.1.0
Yet Another Wake-On-Lan Magic Packet Generator 

## Supported Systems
Developed/Tested on a Mac. Linux support is planned for a future release.

## Usage

    Wolfe .01
    Javier L. Velasquez <nycjv321@gmail.com>
    A WOL Packet Generation Tool
    
    USAGE:
        wolfe [FLAGS] [OPTIONS]
    
    FLAGS:
        -d, --dry-run    don't actually send the packet but print the payload
        -h, --help       Prints help information
        -V, --version    Prints version information
    
    OPTIONS:
        -m, --mac-address <48 Bit MAC Address>    MAC address of host to wake up

## Development

### Building
    
    cargo build --release
    
### Installing

    cp target/release/wolfe /usr/local/bin/
    
## Examples

### Default
    wolfe -m "00:14:22:01:23:45"
    
### Dry Run (Useful for Debugging)
    wolfe -d -m "00:14:22:01:23:45"
    
