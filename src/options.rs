use super::clap::{Arg, App};

pub mod parser {

    pub struct Options {
        pub mac_address:  String,
        pub dry_run:  bool
    }


    pub fn parse() -> Options {
        let matches = super::App::new("Wolfe (🐺)")
            .version(".01")
            .author("Javier L. Velasquez <nycjv321@gmail.com>")
            .about("A WOL Packet Generation Tool")
            .arg(super::Arg::with_name("mac_address")
                .short("m")
                .long("mac-address")
                .value_name("48 Bit MAC Address")
                .help("MAC address of host to wake up")
                .required(false)

                .takes_value(true))
            .arg(super::Arg::with_name("dry-run")
                .short("d")
                .long("dry-run")
                .value_name("dry run")
                .help("don't actually send the packet but print the payload")
                .required(false)
                .takes_value(false))
            .get_matches();


        return Options{
            mac_address: matches.value_of("mac_address").unwrap_or("").to_owned(),
            dry_run: matches.value_of("dry_run").is_some()
        };
    }
}