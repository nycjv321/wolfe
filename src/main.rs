extern crate wolfe;
extern crate pnet;
extern crate ipnetwork;

use std::net::UdpSocket;
use wolfe::configuration;
use wolfe::configuration::broadcast::address;
use wolfe::options::parser;
use wolfe::options::parser::Options;
use std::mem;
use std::process;

use pnet::datalink;
use ipnetwork::IpNetwork;

fn parse(s: &str) -> u8 {
    return u8::from_str_radix(s, 16).expect("");
}

fn copy(s: &str, count: i32) -> Vec<u8> {
    let mut vec: Vec<u8> = Vec::new();
    for _ in 0..count {
        vec.push(parse(s));
    }

    return vec;
}

fn duplicate_mac_address(s: &[&str; 6], count: i32) -> Vec<u8> {
    let mut vec: Vec<u8> = Vec::new();
    for _ in 0..count {
        for j in 0..s.len() {
            let parsed = parse(s[j]);
//            println!("{} to {}", s[j], parsed);
            vec.push(parsed)
        }
    }
    return vec;
}


unsafe fn parse_mac_address(options: &Options) -> [&str; 6] {
    let mac_address = &options.mac_address;
    let split = mac_address.split(":").collect::<Vec<&str>>();
    if split.len() != 6 {
        eprintln!("invalid mac provided: \"{}\"", mac_address);
        process::exit(1);
    }
    let mut address: [&str; 6] = mem::uninitialized();
    for (i, item) in split.iter().enumerate() {
        address[i] = item;
    }
    return address;
}


fn v4_address(networks: &Vec<ipnetwork::IpNetwork>) -> (Option<ipnetwork::IpNetwork>) {
    for network in networks {
        let v4 = match network {
            IpNetwork::V4(_) => true,
            IpNetwork::V6(_) => false
        };
        if v4 {
            return Some(*network);
        }
    }
    return None
}

fn network(interface_name: &String, interfaces:  &Vec<IpNetwork>) -> Option<ipnetwork::IpNetwork> {
    if interface_name.eq(&String::from("en0")) {
        return v4_address(interfaces);
    }
    // skip lo
    if interface_name.eq(&String::from("lo0")) {
        return None;
    }

    // utun0 is created by macOS for VPN and Back to My Mac
    if interface_name.eq(&String::from("utun0")) {
        return None;
    }
    // https://stackoverflow.com/a/28196009
    if interface_name.eq(&String::from("awdl0")) {
        return None;
    }

    // used for tethering
    if interface_name.eq(&String::from("en5")) {
        return None
    }

    return None
}

fn interface_network() -> Option<ipnetwork::IpNetwork> {
    let datalink_interfaces = datalink::interfaces();

    for (index, interface) in datalink_interfaces.iter().enumerate() {
        let interface_name = &interface.name;
        let interfaces = &interface.ips;
        if interfaces.len() == 0 {
            continue;
        }

        let is_mac = cfg!(target_os = "macos");

        if is_mac {
            let value = network(interface_name,interfaces);
            if value.is_some() {
                return value;
            } else {
                continue;
            }
        }

        if index == datalink_interfaces.len() {
            let v4 = v4_address(interfaces);
            if v4.is_none() {
                return None;
            }
        }
    }

    return None
}

fn extract_ip(network: ipnetwork::IpNetwork) -> String {
    return network.ip().to_string()
}

fn socket(source: String) -> UdpSocket {
    let socket = UdpSocket::bind(
        &source)
        .expect("couldn't bind to address");

    socket.set_broadcast(true).expect("set_broadcast call failed");
    return socket;

}

fn create_payload(mac_address:[&str; 6]) -> Vec<u8> {
    let mut prefix = copy("FF", 6);
    let mut message = duplicate_mac_address(&mac_address, 16);

    prefix.append(&mut message);
    return prefix;
}

fn main() {
    let options = parser::parse();
    let mac_address:[&str; 6];
    unsafe {
        mac_address = parse_mac_address(&options);
    }

    let payload = &create_payload(mac_address)[..];
    let destination = address();
    let source = extract_ip(interface_network()
        .expect("unable to determine network")) + ":" + &configuration::udp::port();

    println!("wolfe (🐺) v.0.1.0:");
    println!("trying to awaken {:?} from {:?} over {:?}", mac_address, source, destination);
    println!("payload sent: \n{:?}", payload);

    if !options.dry_run {
        &socket(source)
            .send_to(payload, destination)
            .expect("couldn't send data");
    }
}