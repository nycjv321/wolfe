use std::env;

pub mod application {
    pub fn name() -> String {
        return String::from("wolfe");
    }

}

pub mod broadcast {
    fn ip() -> Option<String> {
        let mut key = super::application::name().to_uppercase();
        key.push_str("_BROADCAST_ADDRESS");
        match super::env::var_os(&key) {
            Some(val) => val.to_str().map(|address| address.to_owned()),
            None => Some(String::from("255.255.255.255"))
        }
    }

    fn port() -> String {
        return String::from("9");
    }

    pub fn address() -> String {
        let mut address = self::ip()
            .expect("unable to determine the broadcast address");

        address.push_str(":");
        address.push_str(&self::port());
        return address;
    }
}
pub mod udp {
    pub fn port() -> String {
        return String::from("2551");
    }
}